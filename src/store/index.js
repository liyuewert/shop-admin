import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
function getState(){
  return {
    token:localStorage.getItem('token')||'',
    username:localStorage.getItem('username')||''
  }
}
export default new Vuex.Store({
  state:getState(),
  mutations: {
    setToken(state,data){
      state.token=data
      localStorage.setItem('token',data)
    },
    setUsername(state,data){
      state.username=data
      localStorage.setItem('username',data)
    },
    initState(state,data){
      Object.assign(state,getState)
      localStorage.clear()
    }
  },
  actions: {
  },
  modules: {
  }
})
