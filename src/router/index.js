import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import store from '../store'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login',
    component: Login
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      noAuthorization: true
    }
  },
  {
    path: '/index',
    name: 'index',
    redirect: '/index/users',
    component: () => import(/* webpackChunkName: "index" */'../views/index.vue'),
    children:
      [
        {
          path: 'users',
          name: 'users',
          component: () => import(/* webpackChunkName: "users" */'../views/users.vue')
        },
        {
          path: 'roles',
          name: 'roles',
          component: () => import(/* webpackChunkName: "roles" */'../views/rights/Roles.vue')
        },
        {
          path: 'rights',
          name: 'rights',
          component: () => import(/* webpackChunkName: "list" */'../views/rights/List.vue')
        },
        {
          path: 'goods',
          name: 'goods',
          component: () => import(/* webpackChunkName: "goods" */'../views/goods/Goods.vue')
        },
        {
          path: 'goodsadd',
          name: 'goodsadd',
          component: () => import(/* webpackChunkName:"goods" */'../views/goods/GoodsAdd.vue')
        },
        {
          path: 'goodsedit/:id',
          name: 'goodsedit',
          component: () => import(/* webpackChunkName:"goods" */'../views/goods/GoodsAdd.vue'),
          props: true
        },
        {
          path: 'params',
          name: 'params',
          component: () => import(/* webpackChunkName: "goods" */'../views/goods/Params.vue')
        },
        {
          path: 'categories',
          name: 'categories',
          component: () => import(/* webpackChunkName: "goods" */'../views/goods/Categories.vue')
        },
        {
          path: 'orders',
          name: 'orders',
          component: () => import(/* webpackChunkName: "orders" */'../views/Orders.vue')
        },
        {
          path: 'reports',
          name: 'reports',
          component: () => import(/* webpackChunkName: "reports" */'../views/Reports.vue')
        },
      ],

  },
]

const router = new VueRouter({
  mode: 'hash',
  routes
})
//路由拦截 (除了是登录页面，其他都有拦截，必须让用户登录在进入其他页面)
router.beforeEach((to, from, next) => {
  if (!to.meta.noAuthorization) {
    if (store.state.token) {
      next()
    } else {
      next({
        path: '/login'
      })
    }
  } else {
    next()
  }
})

export default router
