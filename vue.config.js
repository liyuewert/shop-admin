module.exports = {
  publicPath: (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'test')
    ? '/admin/'
    : '/',
  transpileDependencies: [
    'vue-echarts',
    'resize-detector'
  ],
  pluginOptions: {
    webpackBundleAnalyzer: {
      openAnalyzer: false
    }
  },
  // configureWebpack:{
  //   externals:{
  //     vue:'Vue',
  //     'element'
  //   }
  // }
}