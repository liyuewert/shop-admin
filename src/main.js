import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'  //导入插件
import 'element-ui/lib/theme-chalk/index.css'  //引入css
import 'reset-css'
import {http} from '@/http'
import Myplugin from '../plugins'
import dayjs from 'dayjs'
import ECharts from 'vue-echarts' 
Vue.component('v-chart', ECharts)
import highcharts from 'highcharts'
import VueHighcharts from 'vue-highcharts'
import highcharts3d from 'highcharts/highcharts-3d'
highcharts3d(highcharts)
Vue.use(VueHighcharts)

// import highcharts from 'highcharts'
// import VueHighcharts from 'vue-highcharts'
// import highcharts3d from 'highcharts/highcharts-3d'
// highcharts3d(highcharts)
// Vue.use(VueHighcharts)
Vue.filter('dealdata',(val)=>{
  return dayjs(val).format('YYYY-MM-DD HH:mm:ss')
})
Vue.filter('levelFilter', val => {
  switch (val) {
    case '0' :
     return '一级' 
    case '1' :
      return '二级'
    default: 
      return '三级'
  }
})
Vue.use(ElementUI);
Vue.use(Myplugin);
Vue.config.productionTip = false
Vue.prototype.$http=http

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
