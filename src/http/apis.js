import {http} from './index'
import {Message} from 'element-ui'
//获取菜单权限列表(左侧菜单列表)
 export function getMenus(){
    return http('menus','GET',{}).then(res=>{
         return res.data
     })
 }
 //获取用户列表
 export function getUsers(ruleForm){
     return http('users','GET',{},ruleForm).then(res=>{
         return res.data
         console.log(res)
     })   
 }
 //添加用户
 export function addUsers(userform){
    console.log(123)
    console.log(userform)
    return http('users','POST',userform).then(res=>{
        console.log(456)
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })   
}
//修改用户状态
export function updateUsers(uId,type){
    // users/:uId/state/:type 动态路径 ：UID :type
    return http(`users/${uId}/state/${type}`,'PUT').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//编辑用户提交 obj以对象的形式穿参
export function editUsers(edituserform){
    console.log(edituserform)
    return http(`users/${edituserform.id}`,'PUT',{
        email:edituserform.email,
        mobile:edituserform.mobile
    }).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
    
  
}
//删除用户
export function deleteUsers(id){
    return http(`users/${id}`,'DELETE').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
    
}
//根据id查询用户信息
export function getUsersInfo(id){
    return http(`users/${id}`).then(res=>{
        return res.data
    })
}
//获取角色列表
export function getRolesList(){
    return http('roles','GET').then(res=>{
         return res.data
    })
} 
//分配用户角色
export function distribultRole(id,rid){
    console.log(12243)
    return http(`users/${id}/role`,'PUT',{
        id,
        rid
    }).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//添加角色  
export function addRoles(data){
    console.log(123)
    return http('roles','POST',data).then(res=>{
        Message({
            type:"success",
            message:res.meta.msg
        })
        return res.data
    })   
}
// 角色列表
export function getRoles(){
    return http('roles','GET',{}).then(res=>{
        return res.data
    })   
}
//删除角色指定权限
export function deleteRoles(roleId,rightId){
    console.log(1234)
    return http(`roles/${roleId}/rights/${rightId}`,'DELETE').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
    })
}
//编辑提交角色
export function editRole(obj){
    console.log(1234)
    console.log(obj.id)
    return http(`roles/${obj.id}`,'PUT',{
        roleName:obj.roleName
    }).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//删除角色
export function removeRoles(id){
    return http(`roles/${id}`,'DELETE').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
    
}
//分配权限
export function geteditRole(){
    console.log(1234)
    return http(`rights/tree`,'GET').then(res=>{
        console.log(res)
        return res.data
    })
}
//权限列表
export function geteditRoleList(){
    console.log(1234)
    return http(`rights/list`,'GET').then(res=>{
        console.log(res)
        return res.data
    })
}
//角色授权
export function roleAuthorization(roleId,rids){
     return http(`roles/${roleId}/rights`,'POST',{rids}).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
     })
}
// 商品列表
export function getgoodsList(ruleForm){
    return http('goods','Get',{},ruleForm).then(res=>{
        console.log(res.data)
        return res.data
    })
}
//删除商品列表
export function getRemove(id){
    return http (`goods/${id}`,'DELETE').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
    })
}
//根据id查询商品
export function getDoodsdrtalis(id){
    return http(`goods/${id}`,'GET').then(res=>{
        return res.data
    })
}
//编辑商品列表
export function addeditGoods(obj){
    return http(`goods/${obj.goods_id}`,'PUT',obj).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}

//商品分类列表
export function getCategoriesList(){
    return http(`categories`,'get',{},{type:3}).then(res=>{
        console.log(res.data)
        return res.data
    })
}
//添加商品
export function addGoods(data){
    return http("goods","POST",data).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data

    })
}
//商品参数 
export function getGoodsAttrs(id,sel){
    return http (`categories/${id}/attributes`,'GET',{},{sel}).then(res=>{
        return res.data
    })
}
//获取商品分类参数
export function getGoodsCatAttrs(id,sel){
    return http(`categories/${id}/attributes`,'GET',{},{sel}).then(res=>{
        return res.data
    })
}
//编辑提交参数
export function updateGoodsCatAttrs(id,attrId,data){
    return http(`categories/${id}/attributes/${attrId}`,'PUT',data).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        }) 
        return res.data
    })
}
//删除参数
export function deleteCatAttr(id,attrid){
    console.log(id)
    return http(`categories/${id}/attributes/${attrid}`,'DELETE').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        }) 
    })
}
//设置动态参数
export function setAttr(id,obj){
    console.log(id)
    return http(`categories/${id}/attributes`,'POST',{
        attr_name:obj.attr_name,
        attr_sel:obj.attr_sel,
        attr_vals:obj.attr_vals
    }).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        }) 
    })
}
//获取商品分类
export function getcategories(params){
    console.log(params)
    return http('categories','GET',{},params).then(res=>{
        return res.data
    })
}
//编辑商品提交分类
export function goodscat(id,cat_name){
    return http(`categories/${id}`,'PUT',{cat_name}).then(res=>{
        return res.data
    })
}
//删除商品分类
export function deletecat(id){
    return http(`categories/${id}`,'DELETE').then(res=>{
        return res.data
    })
}
//获取2级的分类列表
export function getcattwo(){
    return http('categories','GET',{},{type:2}).then(res=>{
        return res.data
    })
}
//添加商品分类
export function increaseCat(data){
    return http('categories','POST',data).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        }) 
        return res.data
    })
}
//订单管理
export function getOrders(params){
    return http('orders','GET',{},params).then(res=>{
        return res.data
    })
}
//修改订单状态
export function editOrders(id,data){
    return http(`orders/${id}`,'PUT',data).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        }) 
        return res.data
    })
}
//折线图数据
export function gitLineOptions(){
    console.log(123)
    return http(`reports/type/1`).then(res=>{
        return res.data
    })
}
