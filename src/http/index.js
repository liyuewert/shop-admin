import axios from 'axios'
import { Message } from 'element-ui'
import store from '../store/index'
import router from '../router'
// axios.defaults.baseURL='https://pc.raz-kid.cn/api/private/v1/'
//开发 serve
// let baseURL='http://182.92.117.55:8889/api/private/v1/'
let baseURL;
if(process.env.NODE_ENV==='development'){
  baseURL='http://182.92.117.55:8889/api/private/v1/'
}else if(process.env.NONDE_ENV==="test"){
  baseURL='http://182.92.117.55:8889/api/private/v1/'
}else{
  baseURL='http://182.92.117.55:8889/api/private/v1/'
}
//测试
// let baseURL='http://182.92.117.55:8889/api/private/v1/'

//线上 build
// let baseURL='http://182.92.117.55:8889/api/private/v1/'
// axios.defaults.baseURL = 'http://182.92.117.55:8889/api/private/v1/'
axios.defaults.baseURL=baseURL
//拦截器 axios提供

axios.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  console.log('response', response)
  if (response.data.meta.msg === "无效的token") {
    router.push({
      name: "login"
    })
  }
  return response;
}, function (error) {
  // 对响应错误做点什么
  return Promise.reject(error);
});

export const http = (url, method = 'get', data, params) => {
  return new Promise((resolve, reject) => {
    axios({
      url,
      headers: {
        authorization: store.state.token,
        'content-type': 'application/json'
      },
      method: method.toLocaleLowerCase(),
      data,
      params
    }).then(res => {
      if (res.status >= 200 && res.status < 300 || res.status == 304) {
        if (res.data.meta.status >= 200 && res.data.meta.status < 300) {
          resolve(res.data)
        } else {
          Message.error(res.data.meta.msg)
          reject(res)
        }
      } else {

      }

    }).catch(err => {
      reject(err)
    })
  })
}
